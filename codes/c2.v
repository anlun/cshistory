Theorem eq_trans {T} (x : T) (y : T) (z : T) : x = y -> y = z -> x = z.
Proof.
  intros H1 H2.
  rewrite H1.
  apply H2.
Qed.

