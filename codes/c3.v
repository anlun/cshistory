Theorem eq_trans {T} (x : T) (y : T) (z : T) : x = y -> y = z -> x = z.
Proof. auto. Qed.

